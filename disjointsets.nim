from strutils import format
import tables

type
  DisjointSet*[T] = object
    leaders: Table[T, T]
    ranks: Table[T, int]
    groups: int

proc incl*[T](djset: var DisjointSet[T], key: T) =
  if key notin djset.leaders:
    djset.leaders[key] = key
    djset.ranks[key] = 0
    djset.groups += 1

proc initDisjointSet*[T](): DisjointSet[T] =
  result = DisjointSet[T](leaders: initTable[T, T](), ranks: initTable[T, int](), groups: 0)

proc groupCount*[T](djset: DisjointSet[T]): int =
  djset.groups

proc pathToRoot[T](djset: var DisjointSet[T], key: T): seq[T] =
  result = @[key]

  while result[result.high] != djset.leaders[result[result.high]]:
    result.add(djset.leaders[result[result.high]])

proc compress[T](djset: var DisjointSet[T], key: T) =
  let
    path = djset.pathToRoot(key)
    newLeader = path[path.high]

  for k in path:
    djset.leaders[k] = newLeader

proc findRoot[T](djset: var DisjointSet[T], key: T): T =
  if key notin djset.leaders:
    raise newException(ValueError, "Key '$1' is missing".format(key))
  djset.compress(key)
  return djset.leaders[key]

proc connected*[T](djset: var DisjointSet[T], key1: T, key2: T): bool =
  djset.findRoot(key1) == djset.findRoot(key2)

proc join*[T](djset: var DisjointSet[T], key1: T, key2: T) =
  if not djset.connected(key1, key2):
    let
      r1 = djset.findRoot(key1)
      r2 = djset.findRoot(key2)
    if djset.ranks[r1] > djset.ranks[r2]:
      djset.leaders[r2] = r1
      djset.ranks[r1] = max(djset.ranks[r1], djset.ranks[r2] + 1)
    else:
      djset.leaders[r1] = r2
      djset.ranks[r2] = max(djset.ranks[r2], djset.ranks[r1] + 1)
    djset.groups -= 1
