# Package

version       = "0.1.0"
author        = "Maxim Pertsov"
description   = "Disjoint set (aka Union Find) data structure for Nim"
license       = "BSD3"
skipFiles     = @["tests.nim"]

# Dependencies
requires "nim >= 0.17.3"

task tests, "Run test suite":
    exec "nim c -r tests"
