import unittest
import disjointsets

suite "Union Find":

  test "Basic tests":
    var djset = initDisjointSet[int]()
    for i in [1, 2, 3]:
      djset.incl(i)
    check(not djset.connected(1, 2))
    check(djset.groupCount() == 3)
    djset.join(1, 2)
    check(djset.connected(1, 2))
    check(djset.groupCount() == 2)

  test "With arrays":
    var djset = initDisjointSet[array[0..1, int]]()
    for arr in [[1, 2], [2, 3], [3, 4]]:
      djset.incl(arr)
    check(not djset.connected([1, 2], [2, 3]))
    djset.join([1, 2], [2, 3])
    check(djset.connected([1, 2], [2, 3]))

  test "Connect two groups":
    var djset = initDisjointSet[int]()
    for i in [1, 2, 3, 4]:
      djset.incl(i)

    check(djset.groupCount() == 4)
    djset.join(1, 2)
    check(djset.groupCount() == 3)
    djset.join(3, 4)
    check(djset.groupCount() == 2)
    djset.join(1, 4)
    check(djset.groupCount() == 1)

  test "Connect two array groups":
    var djset = initDisjointSet[array[0..1, int]]()
    for i in [[1, 1], [2, 1], [3, 1], [4, 1]]:
      djset.incl(i)

    check(djset.groupCount() == 4)
    djset.join([1, 1], [2, 1])
    check(djset.groupCount() == 3)
    djset.join([3, 1], [4, 1])
    check(djset.groupCount() == 2)
    djset.join([1, 1], [4, 1])
    check(djset.groupCount() == 1)
